from django.conf import settings

def google_analytics( request ):
    """
    Use the variables returned in this function to
    render your Google Analytics tracking code template.
    """
    context = {}
    if not settings.DEBUG:
        ga_property_id = getattr(settings, 'GOOGLE_ANALYTICS_PROPERTY_ID', False)
        if ga_property_id:
            context['GOOGLE_ANALYTICS_PROPERTY_ID'] = ga_property_id
    return context
