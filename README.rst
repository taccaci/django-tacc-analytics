##############
TACC Analytics
##############

TACC Analytics is a Django app to provide support for various application
analytics and monitoring capabilities.

Currently supported:

- Google Analytics

Quick start
***********

1. Install this module::

    pip install -e https://bitbucket.org/taccaci/django-tacc-analytics.git

2. Add "tacc_analytics" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = (
        ...
        'tacc_analytics',
    )

3. Apply configuration as necessary. The details of this step will be determined
   by which middlewares you select from below.

Google Analytics
****************

Injects the Google Analytics tracking code into the page. The tracking code is not
injected while `DEBUG=True` or if the Property ID is missing from `settings.py`.

Configuration
=============

1. Add the Google Analytics context processor. For Django 1.8 the setting would be::

    TEMPLATES = [
        {
            ...
            'OPTIONS': {
                'context_processors': [
                    ...
                    'tacc_analytics.context_processors.google_analytics',
                ]
            }
        }
    ]

2. Add your Google Analytics Property ID/Tracking ID to settings::

    GOOGLE_ANALYTICS_PROPERTY_ID=UA-XXXXXXXX-X

3. Include the tracking code in your base template::

    {% include 'tacc_analytics/google_analytics/analytics.html' %}

